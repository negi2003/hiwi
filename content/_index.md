---
# Leave the homepage title empty to use the site title
title:
date: 2023-10-22
type: landing

sections:
  - block: hero
    content:
      title: |
        Gamma
        The Coder Hen
      image:
        filename: gamma_tmux.jpg
      text: |
        <br>
        
        The **Coder Hen: Gamma** has been a chicken of excellence for Intelligence, egging, and practice to clean herslef.
  
  - block: collection
    content:
      title: Latest News
      subtitle:
      text:
      count: 5
      filters:
        author: ''
        category: ''
        exclude_featured: false
        publication_type: ''
        tag: ''
      offset: 0
      order: desc
      page_type: post
    design:
      view: card
      columns: '1'
  
  - block: markdown
    content:
      title:
      subtitle: ''
      text:
    design:
      columns: '1'
      background:
        image: 
          filename: coders.jpg
          filters:
            brightness: 
          parallax: false
          position: center
          size: cover
          text_color_light: true
      spacing:
        padding: ['20px', '0', '20px', '0']
      css_class: fullscreen
  
  - block: markdown
    content:
      title:
      subtitle:
      text: |
        {{% cta cta_link="./people/" cta_text="Meet the team →" %}}
    design:
      columns: '1'
---
