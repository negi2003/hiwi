# gamma.gitlab.io

## Most important notes:
- The path of repository should be the same as the `baseURL`
- `baseURL` should be set in [config](config/_default/config.yaml)
- By using group the repository name should be the [`namespace of group`](gamma4091542.gitlab.io)
- BaseURL 
The baseURL in your site configuration must reflect the full URL of your GitLab pages repository if you are using the default GitLab Pages URL (e.g., https://<YourUsername>.gitlab.io/<your-hugo-site>/) and not a custom domain.
CAVE: Backslash at the end of url!


- also go to Deploy -> Pages and uncheck "Use unique domain"
- I have to figure out if i can remove the backslash at the end of the url in the config file to match the url shown in Deploy -> pages. At the moment it is needed to get to the website. 


## Questions:
- **Q:** is it possible to use only public folder and see the website offline? 
  - **A**: yes! simply open it with firefox. You will see the website is not renderd. The reason is the path to css and media, ... are startet with, e.g., `/css` and should be replaced with `/path/to/folder/of/public-folder/css`. 

- **Q:** why Hugo or wowchemy save the path of media from my repo instead of writing them from `public/media`-folder?
  - **A:** it is something related to render-hooks, but i don't know how one can change them! [Here](https://github.com/gohugoio/hugo/issues/10606) is a very hot discussion, but until now, no results -> TBC

## Usefull links
- [The french page](https://infomath.pages.math.cnrs.fr/tutorial/website/) has a nice tutorial for making websites in gitlab-pages

- [This github repository](https://github.com/koenraadvanmeerbeek) use the old version of academic-theme (Researcher group) and the [the website](https://koenraadvanmeerbeek.netlify.app/) is also there. Here the idea is, we could clone this repo and it should work. This guy has also problem with images in `Tour` and has open an [issue](https://discourse.gohugo.io/t/pictures-dont-show-in-slider-or-blank-widget-of-wowchemy/30705) in Hugo, but there is no answer to his issue until now.
